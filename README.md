# README

Before entering into the project I would want to highlight the way I tend to plan a project. Before starting the test, I've created this quick plan to have a vision of how it'll be implemented.

![Services Planning](20210522_123647.jpg)
![Component Planning](20210522_123657.jpg)

This may have changed during implementation, but it gave me a good sense of what I would do and how.

I take this opportunity to attach the plan I've created during my previous role where I've driven a marketing product's tech vision.

[Tech Vision](https://drive.google.com/file/d/1ePXmFIMhDMYy2Ga2EoQMVdwr0AdL-zLv/view?usp=sharing)

NOTE: You'll need to connect your Google account to see all the diagrams! There are 19 ;)

I had to do some minor changes to the JSON provided. I tried to keep it as original as possible.

I would love to use RxJs for the debounce, and API requests instead of Promises. More than anything to replace Promises as a search can be performed in many places at the same time, however, vue3 doesn't provide easy integration with this library, I would have picked vue2 instead :(

## Decisions

I created a project as a single-page application with a single module with many modules. The file structure I followed would be replicated per module of the application if there were more than one.

The application makes use of models as those allow easy testing (unit) usage and extension. The use of modals would force devs to use predefined objects which will maintain data consistency during the entire application lifecycle.

The components folder contains the generic components the module uses.

As the application uses a single layout the pages folder contains the body content components.

The services folder contains all resources consumable by the module implementation. The application will never consume directly from providers. A module can have many services, each consuming different providers.

The providers' folder contains the contract and connection between the application and the API. Providers will request the API and provide a raw output to services. Every API will require its specific provider.

The application uses ESlint & Prettier to maintain code consistency.

The project is set up in bitbucket pipelines for continuous integration.

The project is set up in Netlify for continuous delivery. (Ideally, bitbucket pipelines will deploy the site to AWS/DO or any other infrastructure)

## Pax Video

You can check the app working at [https://suspicious-thompson-dded46.netlify.app/](https://suspicious-thompson-dded46.netlify.app/)

You can check the pipeline passing here https://bitbucket.org/pabloleone/pax/addon/pipelines/home#!/

# Vue 3 + Typescript + Vite

This template should help get you started developing with Vue 3 and Typescript in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur). Make sure to enable `vetur.experimental.templateInterpolationService` in settings!

### If Using `<script setup>`

[`<script setup>`](https://github.com/vuejs/rfcs/pull/227) is a feature that is currently in RFC stage. To get proper IDE support for the syntax, use [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) instead of Vetur (and disable Vetur).

## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can use the following:

### If Using Volar

Run `Volar: Switch TS Plugin on/off` from VSCode command palette.

### If Using Vetur

1. Install and add `@vuedx/typescript-plugin-vue` to the [plugins section](https://www.typescriptlang.org/tsconfig#plugins) in `tsconfig.json`
2. Delete `src/shims-vue.d.ts` as it is no longer needed to provide module info to Typescript
3. Open `src/main.ts` in VSCode
4. Open the VSCode command palette
5. Search and run "Select TypeScript version" -> "Use workspace version"

# Frontend Technical Challenge

Please build a page that allows for searching and filtering of movies via an API. Below is an example response from the API, but the request format is up to you. Our goal is to understand how you would design the API contract so please explain your decisions thoroughly. You do not need to build a functioning API. Mocked or hardcoded responses are perfectly fine.

For the page's overall look & feel, you can use anything you'd like as a reference. Feel free to be inspired by Netflix, IMDB, or other similar websites.

Requirements:

- Use Vue.js, HTML, and any CSS preprocessor of your choice
- The constructed payload must be visible either through the console or some other explicit rendering

The submission should be sent in a GitHub repo.

We expect this to be a well thought out project, but we also understand you may have other job processes or current work/life responsibilities taking up your time. All we ask is that you deliver something that you’re proud of.

Please do not hesitate to reach out and ask for clarification.

Example Response:

```json
[
  {
    "id": 163979287265942016,
    "title": "An American Tail: Fievel Goes West",
    "genre": [
      {
        "id": 3,
        "title": "Comedy"
      },
      {
        "id": 8,
        "title": "Western"
      }
    ],
    "actors": [
      {
        "id": 163978496991957504,
        "name": "Fievel Mousekewitz"
      },
      {
        "id": 163978647315813376,
        "name": "Tanya MouseKewitz"
      }
    ],
    "is_series": true,
    "release_date": "1991-11-17T00:00:00.000000Z"
  },
  {
    "id": 163982121944357888,
    "title": "The Notebook",
    "genre": [
      {
        "id": 2,
        "title": "Drama"
      },
      {
        "id": 6,
        "title": "Romance"
      }
    ],
    "actors": [
      {
        "id": 163982182073900544,
        "name": "Ryan Gosling"
      },
      {
        "id": 163982199253770240,
        "name": "Rachel McAdams"
      }
    ],
    "is_series": false,
    "release_date": "2004-06-25T00:00:00.000000Z"
  }
]
```
