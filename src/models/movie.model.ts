import moment from 'moment'

/**
 * This class serves as a model consumable by the implementation
 */
export default class Movie {
  id: string
  title: string
  genre: Array<any>
  actors: Array<any>
  isSeries: number
  releaseDate: Date

  constructor(
    id: string,
    title: string,
    genre: Array<any>,
    actors: Array<any>,
    isSeries: number,
    releaseDate: Date
  ) {
    this.id = id
    this.title = title
    this.genre = genre
    this.actors = actors
    this.isSeries = isSeries
    this.releaseDate = releaseDate
  }

  getReleaseDate(): string {
    return moment(this.releaseDate).format('MMMM Do YYYY')
  }

  getArtworkUrl(): string {
    return '/img/' + this.id + '.jpeg'
  }
}
