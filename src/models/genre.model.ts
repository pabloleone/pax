/**
 * This class serves as a model consumable by the implementation
 */
export default class Genre {
  id: string
  title: string

  constructor(id: string, title: string) {
    this.id = id
    this.title = title
  }
}
