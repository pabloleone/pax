/**
 * Any HTTP provider must implement this interface
 */
export default interface ProviderInterface {
  get(endpoint: string, params?: any): Promise<any>
  handleError(error: any): void
}
