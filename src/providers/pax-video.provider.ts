import axios from 'axios'
import ProviderInterface from './provider.interface'
import moment from 'moment'

/**
 * This object will contain all communications with the API specific PaxProvider
 * It implements an interface that can be used with any other provider to ensure consistency
 * Axios will perform the HTTP requests and return always a raw json object
 * Any HTTP interceptor will be in between this and the API
 */
export default class PaxVideoProvider implements ProviderInterface {
  url = '/api/json/'

  async get(endpoint: string, params?: any): Promise<Array<any>> {
    const requestUrl: string = this.url + endpoint
    return axios
      .get(requestUrl, params)
      .then((response) => {
        // Filters are applied here as there's no functional backend to perform the filtering
        return response.data.filter((movie: any) => {
          if (
            params.query &&
            !movie.title.toLowerCase().includes(params.query.toLowerCase())
          )
            return false
          if (params.filters) {
            if (
              params.filters.is_series !== -1 &&
              movie.is_series !== params.filters.is_series
            )
              return false
            if (params.filters.actor !== '') {
              let found = false
              movie.actors.forEach((actor: any) => {
                if (
                  actor.name &&
                  actor.name
                    .toLowerCase()
                    .includes(params.filters.actor.toLowerCase())
                )
                  found = true
              })
              if (!found) return false
            }
            if (
              params.filters.release_date.from &&
              moment(movie.release_date).isBefore(
                moment(params.filters.release_date.from)
              )
            )
              return false
            if (
              params.filters.release_date.from &&
              moment(movie.release_date).isAfter(
                moment(params.filters.release_date.to)
              )
            )
              return false

            if (params.filters.genre.length > 0) {
              let found = false
              movie.genre.forEach((genre: any) => {
                params.filters.genre.forEach((lookUpGenre: any) => {
                  if (
                    genre.title &&
                    genre.title
                      .toLowerCase()
                      .includes(lookUpGenre.toLowerCase())
                  )
                    found = true
                })
              })
              if (!found) return false
            }
          }

          return true
        })
      })
      .catch(this.handleError)
  }

  handleError(error: any): void {
    console.error(error)
  }
}
