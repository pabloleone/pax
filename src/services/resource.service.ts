/**
 * This object will contain all resource base methods and configurations
 */
export default abstract class ResourceService {
  public perPage = 10
}
