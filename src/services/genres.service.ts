import Genre from '../models/genre.model'
import PaxVideoProvider from '../providers/pax-video.provider'
import ResourceService from './resource.service'

/**
 * This service will connect the provider wiht the actual implementation making available consumable resources in the
 * form of models
 */
export default class GenresService extends ResourceService {
  async get(params: {
    query?: string
    perPage?: number
  }): Promise<Array<Genre>> {
    params.perPage = this.perPage
    return new PaxVideoProvider()
      .get('genres.json', params)
      .then((response: Array<any>) => {
        return response.map((genre) => new Genre(genre.id, genre.title))
      })
  }
}
