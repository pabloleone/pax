import Movie from '../models/movie.model'
import PaxVideoProvider from '../providers/pax-video.provider'
import ResourceService from './resource.service'

/**
 * This service will connect the provider wiht the actual implementation making available consumable resources in the
 * form of models
 */
export default class MoviesService extends ResourceService {
  async get(params: {
    query?: string
    perPage?: number
    filters?: {
      actor?: string
      genre?: Array<any>
      is_series?: number
      release_date?: { from?: Date; to?: Date }
    }
  }): Promise<Array<Movie>> {
    params.perPage = this.perPage
    return new PaxVideoProvider()
      .get('movies.json', params)
      .then((response: Array<any>) => {
        return response.map(
          (movie) =>
            new Movie(
              movie.id,
              movie.title,
              movie.genre,
              movie.actors,
              movie.is_series,
              movie.release_date
            )
        )
      })
  }
}
