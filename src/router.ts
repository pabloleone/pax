import { createWebHashHistory, createRouter } from 'vue-router'

import Home from './pages/home.page.vue'
import Search from './pages/search.page.vue'

const routes = [
  { path: '/', component: Home, meta: { title: 'Home' } },
  { path: '/search', component: Search, meta: { title: 'Search' } },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router
